# Interrogations

## Memorization

### Points where no model has been trained on

Those points are set to the arbitrary value of ```-100```, maybe ```np.nan``` would be better.

### Choice of constants

The way of choosing those constants is not documented:

- **real_t**: set to 0.3, detection threshold
- **n_counters**: set to 10, number of models trained
- **num_train_epochs**: set to 3, number of epochs

## Extraction

### Need memorization before

Extraction is made on items that have been memorized.

### Dataset_gen

dataset tokenized, each row is divided in section of length **context_length** and the end is deleted.

- a b c d | e f g h | i j (original --> 1 row)
- a b c d, e f g h, i j (intermediate --> 3 rows)
- a b c d, e f g h (final --> 2 rows)

### find_idx

Try to match index from original to new

Original dataset: dataset blue_hoc
New dataset: tokenized dataset (by get_text_gen_dataset) --> same as original but with a max length

If item not find --> return None --> bug afterward

### Choice of constants

The way of choosing those constants is not documented:

- **max_new_tokens**: set to 20, maximum number of tokens generated
- **min_size**: set to 25, minimum number of characters of a text
- **min_words**: set to 7, minimum number of words of a text
- **context_length**: set to 150, number of tokens per text
- **bench_sizes**: set to [0.1,0.25,0.5,0.75], percentage of extracted prompt

## MIA

### Bug MIA real_mems

MIA pipeline crash before the end : X_train = [X[mia_label][k] for k in real_mems] and y_train = [int(y_train[k][0]) for k in real_mems]
Depending on off the size of the dataset, the epoch_bench size

Out of bounds 


Exp 14 :
Parameters : 
epoch_bench = [1]
do_shadow_training = False

Dataset :
100%

Mem 
epochs = 1
n_counters = 1

Result : 
real_mems=[]
No crash

Exp 15 :
Parameters : 
epoch_bench = [1,3,5]
do_shadow_training = False

Dataset :
100%

Mem 
epochs = 1
n_counters = 1

Result : 
real_mems=[]
No crash


Exp 16 :
Parameters : 
epoch_bench = [1,3,5]
do_shadow_training = False

Dataset :
100%

Mem 
epochs = 3
n_counters = 3

Result : 
real_mems=[]
No crash

Exp 17 :
Parameters : 
epoch_bench = [3,5,7]
do_shadow_training = False

Dataset :
100%

Mem 
epochs = 1
n_counters = 1

Result : 
real_mems=[]
No crash