# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 09:53:17 2023

@author: Gaspard
"""

import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import seaborn as sns
import numpy as np
from tqdm.notebook import tqdm
from time import perf_counter

def ordinal_column(df,col,uniques):
    """turns string column into ordinal"""
    df[col] = df[col].apply(lambda x: np.argwhere(uniques==x)[0][0]).astype(int)

def onehot_column(df,col,uniques):
    """turns string column into ordinal"""
    for unique in uniques:
        df[f"{col}_{unique}"] = df[col].apply(lambda x: int(x==unique)).values
    df.drop(columns=[col],axis=1,inplace=True)
    
def clean_columns(df1,df2,df3,onehot=True):
    """renames columns, drops na and turns cols ordinal"""
    column_dict = {
        "age":"age",
        "agechild":"child_age",
        "citistat":"citizenship",
        "female":"genre",
        "married":"married",
        "ownchild":"child_num",
        "wbhaom":"race",
        "gradeatn":"education",
        "cow1":"work_class",
        "ftptstat":"work_status",
        "statefips":"state_code",
        "hoursut":"week_hours",
        "faminc":"fam_income",
        "mind16":"industry",
        "mocc10":"occupation"}
    
    clean_df1 = df1.rename(columns=column_dict)
    clean_df2 = df2.rename(columns=column_dict)
    clean_df3 = df3.rename(columns=column_dict)
    clean_df1.dropna(inplace=True)
    clean_df2.dropna(inplace=True)
    clean_df3.dropna(inplace=True)
    
    for col in ["citizenship","genre","married","race","education","work_class","work_status","state_code","fam_income","industry","occupation","child_age"]:
        uniques1 = np.unique(clean_df1[col])
        uniques2 = np.unique(clean_df2[col])
        uniques3 = np.unique(clean_df3[col])
        uniques = np.sort(np.union1d(uniques3,np.union1d(uniques1,uniques2)))
        if onehot:
            onehot_column(clean_df1,col,uniques)
            onehot_column(clean_df2,col,uniques)
            onehot_column(clean_df3,col,uniques)
        else:
            ordinal_column(clean_df1,col,uniques)
            ordinal_column(clean_df2,col,uniques)
            ordinal_column(clean_df3,col,uniques)
    
    return clean_df1,clean_df2,clean_df3

def get_dfs(folder="main",method="pategan",epsilon="1"):
    """returns clean dataframes"""
    df_raw = pd.read_parquet(Path("data",'base.parquet'), engine='fastparquet')
    df_synth = pd.read_csv(Path("data","public_data_{}".format(folder),"{}_{}_synthetic.csv".format(method,epsilon)))
    df_target = pd.read_csv(Path("data","public_data_{}".format(folder),"{}_{}_targets.csv".format(method,epsilon)))
    dfr,dfs,dft = clean_columns(df_raw,df_synth,df_target)
    return dfr,dfs,dft

def compare_distrib(df_raw,df_syn,col):
    """plots col distribution for both dataframes"""
    fig,ax=plt.subplots(2,1,figsize=(4,6))
    sns.histplot(df_raw[col].values,ax=ax[0]).set(title=f"Raw dataset {col}")
    sns.histplot(df_syn[col].values,ax=ax[1]).set(title=f"Synthetic dataset {col}")
    fig.tight_layout()
    
def get_mean(df,col):
    """returns mean of the col"""
    return round(np.mean(df[col]),2)

def compare_means(df_raw,df_syn,do_plot=False):
    """plots mean differences between dataframes"""
    diffs = []
    cols = list(df_raw.columns)
    cols.remove("child_age")
    for col in cols:
        m2, m1 = get_mean(df_syn,col), get_mean(df_raw,col)
        diff = (m2-m1)/m1
        diffs.append(diff)
    if do_plot:
        plt.bar(x=cols,height=diffs)
        plt.title("Mean differences between raw and synthetic")
        plt.ylabel("% difference")
    return diffs
    
    
def check_is_in(df_raw,test_values):
    """returns int if test in df_raw"""
    for i,values in enumerate(df_raw.values):
        if list(test_values)==list(values):
            return i
    return False


def scalar(v1,v2):
    """scalar product"""
    v1bis = v1#v1[0]+v1[2:]
    v2bis = v2#v2[0]+v2[2:]
    return sum([(v1bis[k]-v2bis[k])**2 for k in range(len(v1bis))])

def angle(v1,v2):
    """cos(angle) between vectors"""
    n1 = sum([v1[k]**2 for k in range(len(v1))])
    n2 = sum([v2[k]**2 for k in range(len(v2))])
    return sum([v1[k]*v2[k] for k in range(len(v1))])/(n1*n2)

def get_scalars(v,df):
    """scalar product of v with all df"""
    N = df.shape[0]
    scalars = []
    for k in range(N):
        v0 = df.values[k]
        scalars.append(scalar(v,v0))
    return scalars

def get_angles(v,df):
    """angle of v with all df"""
    N = df.shape[0]
    angles = []
    for k in range(N):
        v0 = df.values[k]
        angles.append(angle(v,v0))
    return angles

def get_likelihoods(df_t,df_s,balanced=False,do_angle=False):
    """returns normalized scalars"""
    likes = []
    for v in tqdm(df_t.drop(columns=["hhid"],axis=1).values):
        scalars = get_scalars(v,df_s)
        j = np.argmin(scalars)
        distance = scalars[j]
        cos_angle = angle(v,df_s.values[j])
        if do_angle:
            likes.append((1+cos_angle)*distance)
        else:
            likes.append(distance)
    M = max(likes)
    m = min(likes)
    N = len(likes)
    med = np.sort(likes)[int(N/2)]
    for i in range(N):
        if balanced:
            like = likes[i]
            if like<=med:
                likes[i] = 1-0.5*(likes[i]-m)/(med-m)
            else:
                likes[i] = 0.5*(M-likes[i])/(M-med)
        else:
            likes[i] = round((M-likes[i])/(M-m),3)
    return likes
        
    
def do_pred(dfr,dfs,dft,balanced=True,aggr_method="mean",do_angle=False):
    """computes predictions"""
    ind_pred = get_likelihoods(dft,dfs,balanced=balanced,do_angle=do_angle)
    hhids = dft.hhid.values
    cur_id = 0
    h_preds = []
    preds = []
    curr_hh = -1
    hh_count = -1
    for i,ind_id in enumerate(hhids):
        if ind_id!=curr_hh:
            h_preds.append([ind_pred[i]])
            curr_hh = ind_id
            hh_count += 1
        else:
            h_preds[hh_count].append(ind_pred[i])
    for hh in h_preds:
        if aggr_method=="mean":
            preds.append(np.mean(hh))
        elif aggr_method=="median":
            h_scores = np.sort(hh)
            preds.append(h_scores[2])
        elif aggr_method=="max":
            preds.append(np.max(hh))
        elif aggr_method=="min":
            preds.append(np.min(hh))
    return preds
    
    
def pipeline(folder="main",method="pategan",epsilon="1",balanced=True,aggr_method="mean",do_angle=False,do_opposite=False,best_sub=False):
    """saves results for params"""
    t1 = perf_counter()
    dfr,dfs,dft = get_dfs(folder=folder,method=method,epsilon=epsilon)
    preds = np.array(do_pred(dfr,dfs,dft,balanced=balanced,aggr_method=aggr_method,do_angle=do_angle)).astype(float)
    t2 = perf_counter()
    delay = (t2-t1)/60
    print(f"Done in {delay:.2f} min")
    print("Pred size :",len(preds))
    if do_opposite:
        preds = 1-preds
    if best_sub:
        np.savetxt(f"best_sub/{method}_{epsilon}.txt", preds, fmt="%f")
    else:
        np.savetxt(f"balanced{balanced}_{aggr_method}_angle{do_angle}/{method}_{epsilon}.txt", preds, fmt="%f")
    return preds
    
