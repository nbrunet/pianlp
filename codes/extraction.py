from datasets import Dataset

import utils
from extr import extr_grid
from hoc import get_hoc_ds


def do_extraction(ds: Dataset, seed: int, output_dir: str):
    bench_sizes = [0.1, 0.25, 0.5, 0.75]  # Function argument?
    n_trials = 1
    repartition_trains, repartition_tests, _ = extr_grid(n_trials, ds, list(range(50)), bench_sizes, seed=seed,
                                                         context_length=150,  # Why 150??
                                                         do_compute=True, do_training=True,
                                                         max_new_tokens=20,  # Why 20?? or 15??
                                                         min_size=25,  # Why 25??
                                                         min_words=7,  # Why 7??
                                                         save_dir=output_dir, print_graphs=True)

    return repartition_trains, repartition_tests


if __name__ == '__main__':
    tokenizer = utils.get_tokenizer("bert")
    dataset, mean_words, mean_uniques, mean_sizes = get_hoc_ds(tokenizer, 42, print_graphs=False, max_files=-1)
    do_extraction(dataset, 42, "extr")
