## hoc - Gaspard Berthelier
# for hoc dataset utilities

## imports
# technical

# gaspard

# usuals
import re
from time import perf_counter

import matplotlib.pyplot as plt
import numpy as np
# general utils
import pandas as pd
import seaborn as sns
import torch
# verbosity
import transformers

import datasets
import mem
import mia
import training
# personal py files
import utils
from datasets import Dataset

datasets.logging.set_verbosity_error()
transformers.logging.set_verbosity_error()
import os

os.environ["TOKENIZERS_PARALLELISM"] = "false"


# file extraction
def get_text(file_path):
    """returns a string text from file"""
    with open(file_path, encoding="utf8") as text_file:
        texts = text_file.readlines()
    return texts


def remove_str(text, pattern):
    """removes a redundant string pattern in a list of texts"""
    removing = True
    while removing:
        try:
            text.remove(pattern)
        except:
            removing = False
    return text


def get_label(file_path):
    """returns a string label from file"""
    with open(file_path, encoding="utf8") as label_file:
        label = label_file.readlines()[0]

    label = label.split("<")
    for pattern in [" ", ""]:
        label = remove_str(label, pattern)
    label = " ".join(label)

    label = label.split(" ")
    for pattern in [" ", ""]:
        label = remove_str(label, pattern)
    label = " ".join(label)

    return label


def get_hoc(max_files=-1):
    """returns hoc dataset as text and labels strings"""
    texts = []
    labels = []
    os.chdir("text")
    number_of_text_files = 0
    for file in os.listdir():
        if number_of_text_files == max_files:
            break
        if file.endswith(".txt"):
            number_of_text_files += 1
            file_path = f"{file}"
            texts.append(get_text(file_path))

    number_of_label_files = 0
    os.chdir("../labels")
    for file in os.listdir():
        if number_of_label_files == max_files:
            break
        if file.endswith(".txt"):
            number_of_label_files += 1
            file_path = f"{file}"
            labels.append(get_label(file_path))

    os.chdir("..")

    if number_of_text_files != number_of_label_files:
        raise FileNotFoundError(f"There are not the same number of text files ({number_of_text_files})"
                                f"as the number of label files ({number_of_label_files})")

    return texts, labels


def label_cleaning(label):
    """seperates mutli labels from label string"""
    delimiters = "--", "AND", "and", "  "
    regex_pattern = '|'.join(map(re.escape, delimiters))
    labels = re.split(regex_pattern, label)
    for i, label in enumerate(labels):
        labels[i] = label.strip()
    remove_str(labels, " ")
    remove_str(labels, "")
    output = list(dict.fromkeys(labels))
    return output if output != [] else None


hallmarks = ["Sustaining proliferative signaling", "Evading growth suppressors", "Resisting cell death",
    "Enabling replicative immortality", "Inducing angiogenesis", "Activating invasion & metastasis",
    "Genome instability & mutation", "Tumor-promoting inflammation", "Deregulating cellular energetics",
    "Avoiding immune destruction"]

hallmarks2 = ["proliferative signaling", "growth suppressors", "cell death", "replicative immortality", "angiogenesis",
    "invasion", "instability", "tumor-promoting", "deregulating", "avoiding"]

hallmarks3 = ["proliferative", "suppressors", "cell death", "immortality", "angiogenesis", "metastasis", "mutation",
    "inflammation", "energetics", "destruction"]


def get_hallmarks():
    """returns hallmark names"""
    return hallmarks


# dataset

def detect_label(labels):
    """returns one hot vector of label presence"""
    label_presence = [0 for k in range(len(hallmarks))]
    for label in labels:
        for hallmark_dict in [hallmarks, hallmarks2, hallmarks3]:
            for i, hallmark in enumerate(hallmark_dict):
                if hallmark in label:
                    label_presence[i] = 1
    return label_presence


def get_mono_dfds(tokenizer, seed, print_info=True, max_files=-1):
    """returns cleaned hoc dataset"""
    print("Building dataset")
    # Import of data from the text/ and labels/ folder
    # texts is a list of list, each sub list contains a sentence of
    # the corresponding abstract
    # labels is a list of string, containing the description of the abstract
    texts, labels = get_hoc(max_files=max_files)
    df = pd.DataFrame({"text": texts, "labels": labels})

    # Each string is divided in a list of string of different small label
    df["labels"] = df["labels"].apply(lambda x: label_cleaning(x))  # list of strings.
    df["label_presence"] = df["labels"].apply(lambda x: detect_label(x) if x else None)  # one hot vector
    df["label_counts"] = df["label_presence"].apply(lambda x: np.sum(x) if x else None)  # int
    df.drop(np.where(df["label_counts"] == 0)[0], axis=0, inplace=True)  # remove lines without labels
    df.reset_index(drop=True, inplace=True)
    df.drop(np.where(df["label_presence"].values == None)[0], axis=0, inplace=True)
    df.reset_index(drop=True, inplace=True)
    df.drop(np.where(df["label_counts"].values == None)[0], axis=0, inplace=True)
    df.reset_index(drop=True, inplace=True)
    df["text"] = df["text"].apply(lambda x: " ".join(x))  # Each row is now a single string
    df["word_count"] = df["text"].apply(lambda x: len(x.split(" ")))
    df["unique_words"] = df["text"].apply(lambda x: len(np.unique(x.split(" "))))
    df["chr_count"] = df["text"].apply(lambda x: len(x))
    N = df.shape[0]
    df[hallmarks] = [df["label_presence"].values[k] for k in range(N)]  # one hot column for each label
    mono_idx = np.where(df["label_counts"] == 1)[0]
    df_mono = df.iloc[mono_idx]  # Contains only abstract (and label) that has only one label
    # TODO voir s'il faut ajouter un reset index après la supression des points avec multi label
    dataset = Dataset.from_dict(
        {"text": df["text"].values, "label": df["label_presence"].values})  # labels are one-hot list
    dataset = utils.tokenize_dataset(dataset, tokenizer)
    dataset_mono = dataset.select(mono_idx)
    dataset_mono = dataset_mono.map(lambda data: {"label": np.argmax(data["label"])})  # ordinal label
    dataset_mono = dataset_mono.shuffle(seed=seed)

    if print_info:
        utils.check_dataset(dataset_mono)
        long_idx = np.where(np.array(utils.get_num_tokens(dataset_mono["input_ids"])) == 512)[0]
        print(f"Truncated {len(long_idx)} sequences")

    return dataset_mono, df_mono


def plot_mono_dfds(dataset_mono, df_mono, hallmarks):
    """plots dataset info"""
    fig, axes = plt.subplots(2, 3, figsize=(15, 7))
    sns.histplot(ax=axes[0, 0], x=df_mono["word_count"]).set(title="Number of words distribution")
    sns.histplot(ax=axes[0, 1], x=df_mono["unique_words"]).set(title="Number of unique words distribution")
    sns.histplot(ax=axes[1, 0], x=utils.get_num_tokens(dataset_mono["input_ids"])).set(
        title="Number of tokens distribution")
    sns.histplot(ax=axes[1, 1], x=utils.get_unique_tokens(dataset_mono["input_ids"])).set(
        title="Number of unique tokens distribution")
    sns.histplot(ax=axes[0, 2], x=df_mono["chr_count"]).set(title="Number of characters distribution")
    label_counts = {key: sum(df_mono[key].values) for key in hallmarks}
    sns.barplot(ax=axes[1, 2], x=[str(k) for k in range(len(hallmarks))],
                y=[label_counts[key] for key in hallmarks]).set(title="Labels distribution")
    plt.tick_params(axis='x', rotation=90)
    fig.tight_layout()


def get_hoc_ds(tokenizer, seed, print_graphs=False, return_means=True, max_files=-1):
    """returns hoc dataset (max_files=-1 give all the available files)"""
    dataset_mono, df_mono = get_mono_dfds(tokenizer, seed, print_info=print_graphs, max_files=max_files)
    if print_graphs:
        plot_mono_dfds(dataset_mono, df_mono, get_hallmarks())
    mean_words = int(np.mean(df_mono["word_count"]))
    mean_uniques = int(np.mean(df_mono["unique_words"]))
    mean_size = int(np.mean(df_mono["chr_count"]))
    if print_graphs:
        print("Mean number of word : ", mean_words)
        print("Mean number of unique words : ", mean_uniques)
        print("Mean number of characters : ", mean_size)
    if return_means:
        return dataset_mono, mean_words, mean_uniques, mean_size
    else:
        return dataset_mono


def get_all_hoc(seed=42, real_t=0.3, print_graphs=True, model_name="bert", include_mem=True, compute_mem=False,
                mem_dir="mem", max_files=-1):
    """returns dataset memorization and other info"""

    # Training parameters
    epochs = 2
    n_counters = 2

    print("get_tokenizer")
    tokenizer = utils.get_tokenizer(model_name)
    print("get_hoc_ds")
    dataset, mean_words, mean_uniques, mean_sizes = get_hoc_ds(tokenizer, seed, print_graphs=print_graphs,
                                                               max_files=max_files)
    means = (mean_words, mean_uniques, mean_sizes)
    id2label, label2id = utils.get_label_dict(10)
    label_dicts = [id2label, label2id]
    print("mem_pipeline")
    real_mems = mem.mem_pipeline(dataset, tokenizer, label_dicts, means, real_t=real_t, n_counters=n_counters,
                                 do_train=compute_mem, do_perf=compute_mem, model_dir=mem_dir, seed=seed,
                                 num_train_epochs=epochs, print_results=print_graphs)

    dataset = mia.get_in_out(dataset, real_mems, seed=seed, out_size=0.5, include_mem=include_mem)
    return dataset, real_mems, tokenizer, label_dicts


def main():
    print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-"
          "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-")
    bench_sizes = [0.1, 0.25, 0.5, 0.75]
    n_trials = 5
    do_compute = False
    seed = 40

    # for reproducible results
    seed = 40
    node = "taurus-4"
    site = "nancy"

    n_counters = 3
    n_splits = 4
    random_state = 42
    test_size = 0.25

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    # check current dir
    print("Current dir :", os.getcwd(), flush=True)

    T1 = perf_counter()
    dataset, real_mems, tokenizer, label_dicts = get_all_hoc(seed=seed, real_t=0.3, print_graphs=True, compute_mem=True)

    print(f"{len(dataset)=}", flush=True)

    do_target_train = True
    epoch_bench = [3,5,7]

    training.train_grid(dataset.filter(lambda data: data["status"] == 1),
                        dataset.filter(lambda data: data["status"] == 0), tokenizer, epoch_bench, label_dicts,
                        target_dir="target_model", seed=seed, compute=do_target_train, print_graphs=True, start_at=0, )
    target_dir = f"target_model/model_{epoch_bench[-1]}"
    print("fin train_grid", flush=True)
    # score separability of target
    print("debut separability", flush=True)
    mia.draw_separability(dataset, tokenizer, "target_model", epoch_bench, label_dicts, "prediction_score")
    print("fin separability", flush=True)

    print("debut get_shadow_dataset", flush=True)
    # add predictions of latest model
    dataset = mia.get_shadow_dataset(dataset, tokenizer, target_dir, label_dicts)
    print("fin get_shadow_dataset")

    # shoadw model on separate scores
    do_shadow_training = False

    print("debut mia.pipeline")
    train_tests, xgb_model, X, y_train, dataset, acc, mem_acc = mia.mia_pipeline(tokenizer, dataset, seed, label_dicts,
                                                                                 real_mems, sh_label_name="true_label",
                                                                                 mia_label="separate_score",
                                                                                 n_shadows=1, num_epochs=2,
                                                                                 train_type="simple", pos_weight=2,
                                                                                 do_shadows_training=do_shadow_training,
                                                                                 shadow_dir="mia",
                                                                                 build_mia=do_shadow_training,
                                                                                 print_graphs=True, print_report=True)

    print('fin mia.pipeline')
    # utils.reload(mem)
    # model_name = "bert"
    # tokenizer = utils.get_tokenizer(model_name)
    # dataset, mean_words, mean_uniques, mean_sizes = get_hoc_ds(tokenizer, seed, return_means=True)
    # means = (mean_words, mean_uniques, mean_sizes)
    # id2label, label2id = utils.get_label_dict(10)
    # label_dicts = [id2label, label2id]
    # from sklearn.model_selection import ShuffleSplit
    #
    # rs = ShuffleSplit(n_splits=n_splits, test_size=test_size, random_state=random_state)
    # train_tests = []  # for train_index, test_index in rs.split(dataset):
    #     train_tests.append({"in":train_index,"out":test_index})
    # print(len(train_tests))


if __name__ == "__main__":
    main()
