from typing import Callable

import utils
from datasets import Dataset
from extr import extr_grid
from load_dataset import load_hoc, load_sst2
from mem import mem_pipeline
from mia import get_in_out, draw_separability, get_shadow_dataset, mia_pipeline
from training import train_grid


def run(dataset: Dataset, tokenizer: Callable, seed: int, output_dir_prefix: str, means: tuple[float, float, float],
        labels: list[dict], bench_sizes: list[float], epoch_bench: list[int]) -> None:
    """Run the memorization, then the extraction and finally the MIA.

    :param dataset: dataset on which the analysis is performed.
    :param tokenizer: tokenizer used to tokenize the dataset.
    :param seed: seed used for reproducible results.
    :param output_dir_prefix: prefix directory to store results (usually dataset name).
    :param means: means returned by the dataset loader.
    :param labels: labels dictionary returned by the dataset loader.
    :param bench_sizes: percentage of extracted prompt.
    :param epoch_bench: epochs number.
    """
    # Memorization
    real_memos = mem_pipeline(dataset, tokenizer, labels, means, real_t=0.3, n_counters=10, do_train=True, do_perf=True,
                              model_dir=f"{output_dir_prefix}_mem", seed=seed, num_train_epochs=3, print_results=True)

    dataset = get_in_out(dataset, real_memos, seed=seed, out_size=0.5, include_mem=True)

    # Extraction
    repartition_trains, repartition_tests, _ = extr_grid(1, dataset, real_memos, bench_sizes, seed=seed,
                                                         context_length=150, do_compute=True, do_training=True,
                                                         max_new_tokens=20, min_size=25, min_words=7,
                                                         save_dir=f"{output_dir_prefix}_extr", print_graphs=True)

    # MIA
    train_grid(dataset.filter(lambda data: data["status"] == 1), dataset.filter(lambda data: data["status"] == 0),
               tokenizer, epoch_bench, labels, target_dir=f"{output_dir_prefix}_target_model", seed=seed, compute=True,
               print_graphs=True, start_at=0, )

    draw_separability(dataset, tokenizer, f"{output_dir_prefix}_target_model", epoch_bench, labels, "prediction_score")

    dataset = get_shadow_dataset(dataset, tokenizer, f"{output_dir_prefix}_target_model/model_{epoch_bench[-1]}",
                                 labels)

    mia_pipeline(tokenizer, dataset, seed, labels, real_memos, sh_label_name="true_label", mia_label="separate_score",
                 n_shadows=1, num_epochs=2, train_type="simple", pos_weight=2, do_shadows_training=True,
                 shadow_dir=f"{output_dir_prefix}_mia", build_mia=True, print_graphs=True, print_report=True)


def config():
    """Configuration of the analysis."""
    tokenizer = utils.get_tokenizer("bert")
    seed = 42
    bench_sizes = [0.1, 0.25, 0.5, 0.75]
    epoch_bench = [1, 3, 5, 7]

    return tokenizer, seed, bench_sizes, epoch_bench


def main_hoc():
    """Launcher for hoc dataset"""
    tokenizer, seed, bench_sizes, epoch_bench = config()

    dataset_hoc, means_hoc, labels_dicts_hoc = load_hoc(tokenizer, seed)
    run(dataset_hoc, tokenizer, seed, "hoc", means_hoc, labels_dicts_hoc, bench_sizes, epoch_bench)


def main_sst2():
    """Launcher for sst2 dataset"""
    tokenizer, seed, bench_sizes, epoch_bench = config()

    dataset_sst2, means_sst2, labels_dicts_sst2 = load_sst2(tokenizer, seed)
    run(dataset_sst2, tokenizer, seed, "sst2", means_sst2, labels_dicts_sst2, bench_sizes, epoch_bench)


if __name__ == '__main__':
    main_hoc()
