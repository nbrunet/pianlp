import numpy as np
import pandas as pd
from datasets import load_dataset, Dataset

from hoc import get_hoc_ds
from utils import get_label_dict, tokenize_dataset


def load_hoc(tokenizer, seed, max_files=-1):
    """Load hoc dataset.

    :param tokenizer: tokenizer used to tokenize the dataset.
    :param seed: seed used for reproducible results.
    :param max_files: maximum number of files used to load the dataset (-1 for no limit).
    """
    dataset_mono, mean_words, mean_uniques, mean_sizes = get_hoc_ds(tokenizer, seed=seed, return_means=True,
                                                                    max_files=max_files)
    means = (mean_words, mean_uniques, mean_sizes)
    id2label, label2id = get_label_dict(10)
    label_dicts = [id2label, label2id]

    return dataset_mono, means, label_dicts


def load_sst2(tokenizer, seed):
    """Load sst2 dataset.

    :param tokenizer: tokenizer used to tokenize the dataset.
    :param seed: seed used for reproducible results.
    """
    dataset = load_dataset("sst2")
    df = pd.DataFrame(dataset.get("train"))

    df["word_count"] = df["sentence"].apply(lambda x: len(x.split(" ")))
    df["unique_words"] = df["sentence"].apply(lambda x: len(np.unique(x.split(" "))))
    df["chr_count"] = df["sentence"].apply(lambda x: len(x))

    mean_words = int(np.mean(df["word_count"]))
    mean_uniques = int(np.mean(df["unique_words"]))
    mean_size = int(np.mean(df["chr_count"]))

    means = (mean_words, mean_uniques, mean_size)

    dataset = Dataset.from_dict({"text": df["sentence"].values, "label": df["label"].values})
    dataset = tokenize_dataset(dataset, tokenizer)
    dataset = dataset.shuffle(seed=seed)

    label_dicts = [{"0": "negative", "1": "positive"}, {"negative": "0", "positive": "1"}]

    return dataset, means, label_dicts
