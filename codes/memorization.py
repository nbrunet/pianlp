from mem import mem_pipeline
from hoc import get_all_hoc


def memorization(dataset,tokenizer,label_dicts,means,n_counters,compute_mem=True,model_dir='mem'):

    real_mems = mem_pipeline(dataset, tokenizer, label_dicts, means, real_t=real_t, n_counters=n_counters,
                                 do_train=compute_mem, do_perf=compute_mem, model_dir=mem_dir, seed=seed,
                                 num_train_epochs=3, print_results=print_graphs)