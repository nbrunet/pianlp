from typing import Callable

from datasets import Dataset

import utils
from load_dataset import load_hoc, load_sst2
from mia import get_in_out
from extr import extr_grid
from mem import mem_pipeline


def do_both(ds: Dataset, tokenizer: Callable, seed: int, output_dir_prefix: str, mean: tuple[float, float, float], labels: list[dict]):
    bench_sizes = [0.1, 0.25, 0.5, 0.75]  # Function argument?
    n_trials = 5

    real_mems = mem_pipeline(ds, tokenizer, labels, mean, real_t=0.3, n_counters=10, do_train=True, do_perf=True,
                             model_dir=f"{output_dir_prefix}_mem", seed=seed, num_train_epochs=3, print_results=True)

    ds = get_in_out(ds, real_mems, seed=seed, out_size=0.5, include_mem=True)

    repartition_trains, repartition_tests, _ = extr_grid(n_trials, ds, real_mems, bench_sizes, seed=seed,
                                                         context_length=150,  # Why 150??
                                                         do_compute=True, do_training=True, max_new_tokens=20,
                                                         # Why 20?? or 15??
                                                         min_size=25,  # Why 25??
                                                         min_words=7,  # Why 7??
                                                         save_dir=f"{output_dir_prefix}_extr", print_graphs=True)

    return repartition_trains, repartition_tests


if __name__ == '__main__':
    TOKENIZER = utils.get_tokenizer("bert")
    SEED = 42

    # dataset_hoc, means_hoc, labels_dicts_hoc = load_hoc(tokenizer, SEED, max_files=-1)
    # do_both(dataset_hoc, TOKENIZER, SEED, "hoc", means_hoc, labels_dicts_hoc)

    dataset_sst2, means_sst2, labels_dicts_sst2 = load_sst2(TOKENIZER, SEED)
    do_both(dataset_sst2, TOKENIZER, SEED, "sst2", means_sst2, labels_dicts_sst2)
